import React, { useState, useEffect } from 'react';
import axios from 'axios';
import $ from "jquery";
import './App.css';

const URL = 'http://localhost:3000/characters'

const App = () => {
    const [characters, setCharacters] = useState([])

    useEffect(() => {
        getData()
    }, [])

    const getData = async () => {
        const response = await axios.get(URL)
        setCharacters(response.data)
    }



    // const params = new URLSearchParams();
    // params.append('id', ${id});
    // params.append('name', ${name});
    // params.append('age', ${age});
    // params.append('occupation', ${occupation});
    //
    // //Edit button code
    // const editData = (id) => axios({
    //   method: 'post',
    //   url: URL,
    //   data: params
    // }).then(res => {
    //         const patch = characters.filter(character => id !== character.id)
    //         setCharacters(patch)
    // })


    // //Edit button code
    // const editData = (id) => {
    //
    //     axios.post(`${URL}/${id}`).then(res => {
    //         const patch = characters.filter(character => id !== character.id)
    //         setCharacters(patch)
    // })
    // }

    // const editData = (id) => {
    //   axios({
    //   method: 'post',
    //   url: URL,
    //   characters: {
    //     id: id.rowValue,
    //     name: name.rowValue,
    //     age: age.rowValue,
    //     occupation: occupation.rowValue
    //   }
    // })
    // .then(res => {
    //       const patch = characters.filter(character => id !== character.id)
    //       setCharacters(patch)
    //   })
    // }



        // const editData = (id) => {
        //   // for(var i = 0, length = this.table.tbody.length; i < length; i++) {
        //   var c1=this.table.tbody[0].value;
        //   var c2=this.table.tbody[1].value;
        //   var c3=this.table.tbody[2].value;
        //   var c4=this.table.tbody[3].value;
        // // }
        //
        //   axios({
        //   method: 'post',
        //   url: URL,
        //   characters: {
        //     id: c1,
        //     name: c2,
        //     age: c3,
        //     occupation: c4
        //   }
        // })
        // .then(res => {
        //       const patch = characters.filter(character => id !== character.id)
        //       setCharacters(patch)
        //   })
        // }




    const editData = (id) => {
        axios.patch(`${URL}/${id}`).then(res => {
            const patch = characters.filter(character => id !== character.id)
            setCharacters(patch)
        })
    }



const addRow = (id) => {
  axios({
  method: 'post',
  url: URL,
  characters: {
    id: '',
    name: '',
    age: '',
    occupation: ''
  }
})
.then(res => {
      const add = characters.filter(character => id !== character.id)
      setCharacters(add)
  })
}


    const removeData = (id) => {
        axios.delete(`${URL}/${id}`).then(res => {
            const del = characters.filter(character => id !== character.id)
            setCharacters(del)
        })
    }

    const renderHeader = () => {
        let headerElement = ['id', 'name', 'age', 'occupation', 'Buttons']

        return headerElement.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    const renderBody = () => {
        return characters && characters.map(({ id, name, age, occupation }) => {
            return (
                <tr key={id}>
                    <td contenteditable='true'>{id}</td>
                    <td contenteditable='true'>{name}</td>
                    <td contenteditable='true'>{age}</td>
                    <td contenteditable='true'>{occupation}</td>
                    <td className='Buttons'>
                        <button className='button' onClick={()=>editData(id)}>Edit</button>
                        <button className='button' onClick={() => removeData(id)}>Delete</button>
                        <button className='button' onClick={()=>addRow(id)}>New_entry</button>
                    </td>
                </tr>
            )
        })
    }

    return (
        <>
            <h1 id='title'>Mistborn's main characters</h1>
            <table id='character'>
                <thead>
                    <tr>{renderHeader()}</tr>
                </thead>
                <tbody>
                    {renderBody()}
                </tbody>
            </table>
        </>
    )
}


// $("td").click(function(){
//     if($(this).attr("contentEditable") == true){
//         $(this).attr("contentEditable","false");
//     } else {
//         $(this).attr("contentEditable","true");
//     }
// })





export default App;
